package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Diarydear
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCheckWin_X_Row1_output_true(){
        char [][]table = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true, result);
    }  
    @Test
    public void testCheckWin_X_Row2_output_true(){
        char [][]table = {{'-','-','-'},{'X','X','X'},{'-','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true, result);
    } 
    @Test
    public void testCheckWin_X_Row3_output_true(){
        char [][]table = {{'-','-','-'},{'-','-','-'},{'X','X','X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Row_1_output_true(){
        char [][]table = {{'O','O','O'},{'-','-','-'},{'-','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Row_2_output_true(){
        char [][]table = {{'-','-','-'},{'O','O','O'},{'-','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Row_3_output_true(){
        char [][]table = {{'-','-','-'},{'-','-','-'},{'O','O','O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Row1_output_false(){
        char [][]table = {{'O','O','X'},{'X','X','-'},{'O','O','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWin_O_Row2_output_false(){
        char [][]table = {{'-','-','X'},{'X','O','X'},{'O','O','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWin_O_Row_output_false(){
        char [][]table = {{'O','-','X'},{'-','O','X'},{'X','O','X'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWin_O_Col1_output_true() {
        char[][] table = {{'O', '-', '-'}, {'O', '-', '-'}, {'O', '-', '-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Col2_output_true() {
        char[][] table = {{'-', 'O', '-'}, {'-', 'O', '-'}, {'-', 'O', '-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Col3_output_true() {
        char[][] table = {{'-', '-', 'O'}, {'-', '-', 'O'}, {'-', '-', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Col1_output_false() {
        char[][] table = {{'X', '-', 'O'}, {'O', '-', 'O'}, {'X', '-', '-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWin_O_Col2_output_false() {
        char[][] table = {{'X', 'O', '-'}, {'-', '-', 'O'}, {'X', '-', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWin_O_Col3_output_false() {
        char[][] table = {{'-', '-', 'X'}, {'-', 'X', 'O'}, {'X', '-', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
    @Test
    public void testCheckDraw_X_output_true() {
        char[][] table = {{'X','O','O'},{'X','O','O'},{'X','O','X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkDraw(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Diagonal_left_output_true(){
        char[][] table = {{'X','-','-'},{'-','X','-'},{'-','-','X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin_Diagonal(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Diagonal_right_output_true(){
        char[][] table = {{'-','-','X'},{'-','X','-'},{'-','-','X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Diagonal_left_output_false(){
        char[][] table = {{'-','X','O'},{'-','O','-'},{'X','-','O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin_Diagonal(table, currentPlayer);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWin_O_Diagonal_right_output_false(){
        char[][] table = {{'-','-','O'},{'-','-','O'},{'O','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }
}
