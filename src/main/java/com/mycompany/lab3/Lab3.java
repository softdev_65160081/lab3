/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3;

/**
 *
 * @author Diarydear
 */
public class Lab3 {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
    
    static boolean checkWin(char[][] table, char currentPlayer){
        for(int i=0;i<3;i++){
            if(table[i][0]==currentPlayer && table[i][1]==currentPlayer && table[i][2]==currentPlayer){
            return true;
            }
            if(table[0][i]==currentPlayer && table[1][i]==currentPlayer && table[2][i]==currentPlayer){
            return true;
            }
            if(table[0][2]==currentPlayer && table[1][1]==currentPlayer && table[2][1]==currentPlayer){
            return true;
            }
        }
        return false;
    }
    
    static boolean checkWin_Diagonal(char[][] table, char currentPlayer){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(table[i][i]!=currentPlayer){
                return false;
                }
            }
        }  
        return true;
    }
    static boolean checkDraw(char[][] table, char currentPlayer) {
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(table[i][j]=='-'){
                return false;
                }
            }
        }
        return true;   
    }
}
